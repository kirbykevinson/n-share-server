using System.Data.SQLite;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System;

namespace n_share_server {
	public struct NFileInfo {
		public string Name;
		public string Description;
		public int DownloadsLeft;
		public int Size;
	}
	
	public struct NTrackingInfo {
		public string Ip;
		public string Ua;
		public string Date;
	}
	
	public enum NDeletionResult {
		Success,
		DoesntExist,
		WrongMagic
	}
	
	public struct NJustice {
		public string Magic;
		public NTrackingInfo Track;
	}
	
	public class NDb {
		SQLiteConnection Connection;
		
		RNGCryptoServiceProvider Random;
		
		public NDb() {
			Connection = new SQLiteConnection("Data Source=n.db");
			
			Random = new RNGCryptoServiceProvider();
		}
		
		public void Run() {
			Connection.Open();
			
			using var command = new SQLiteCommand(Connection);
			
			command.CommandText = @"
				create table if not exists files (
					id text primary key,
					data text,
					magic text,
					
					name text,
					description text,
					downloads_left int,
					size int
				);
				
				create table if not exists tracking (
					id text primary key,
					ip text,
					ua text,
					date text
				);
			";
			
			command.ExecuteNonQuery();
		}
		
		string GenerateRandomId() {
			var randomBytes = new byte[32];
			
			Random.GetBytes(randomBytes);
			
			return Convert.ToBase64String(randomBytes);
		}
		
		public async Task<string> Upload(
			string data,
			string magic,
			NFileInfo info,
			NTrackingInfo track
		) {
			using var command = new SQLiteCommand(Connection);
			
			var id = GenerateRandomId();
			
			command.CommandText = @"
				insert into files values (
					@id,
					@data,
					@magic,
					
					@name,
					@description,
					@downloads_left,
					@size
				);
				
				insert into tracking values (
					@id,
					@ip,
					@ua,
					datetime('now')
				);
			";
			
			command.Parameters.AddWithValue("@id", id);
			command.Parameters.AddWithValue("@data", data);
			command.Parameters.AddWithValue("@magic", magic);
			command.Parameters.AddWithValue("@name", info.Name);
			command.Parameters.AddWithValue("@description", info.Description);
			command.Parameters.AddWithValue(
				"@downloads_left", info.DownloadsLeft
			);
			command.Parameters.AddWithValue("@size", info.Size);
			command.Parameters.AddWithValue("@ip", track.Ip);
			command.Parameters.AddWithValue("@ua", track.Ua);
			command.Prepare();
			
			await command.ExecuteNonQueryAsync();
			
			return id;
		}
		
		public async Task<NFileInfo?> Info(string id) {
			using var command = new SQLiteCommand(Connection);
			
			command.CommandText = @"
				select
					name,
					description,
					downloads_left,
					size
				from files where id = @id;
			";
			
			command.Parameters.AddWithValue("@id", id);
			command.Prepare();
			
			using var reader = await command.ExecuteReaderAsync();
			
			if (!reader.Read()) {
				return null;
			}
			
			return new NFileInfo() {
				Name = reader.GetString(0),
				Description = reader.GetString(1),
				DownloadsLeft = reader.GetInt32(2),
				Size = reader.GetInt32(3)
			};
		}
		
		public async Task<NDeletionResult> Delete(string id, string magic) {
			using var command = new SQLiteCommand(Connection);
			
			command.CommandText = @"
				select magic from files where id = @id;
				delete from files where id = @id and magic = @magic;
			";
			
			command.Parameters.AddWithValue("@id", id);
			command.Parameters.AddWithValue("@magic", magic);
			command.Prepare();
			
			var realMagic = await command.ExecuteScalarAsync();
			
			if (realMagic == null) {
				return NDeletionResult.DoesntExist;
			}
			
			if ((string) realMagic != magic) {
				return NDeletionResult.WrongMagic;
			}
			
			return NDeletionResult.Success;
		}
		
		public async Task<string> Download(string id) {
			using var command = new SQLiteCommand(Connection);
			
			command.CommandText = @"
				select data from files where id = @id;
				
				update files
					set downloads_left = downloads_left - 1
					where id = @id;
				delete from files where id = @id and downloads_left < 1;
			";
			
			command.Parameters.AddWithValue("@id", id);
			command.Prepare();
			
			return (string) await command.ExecuteScalarAsync();
		}
		
		public async Task<NJustice?> ExecuteJustice(string id) {
			using var command = new SQLiteCommand(Connection);
			
			command.CommandText = @"
				select magic from files where id = @id;
				select ip, ua, date from tracking where id = @id;
			";
			
			command.Parameters.AddWithValue("@id", id);
			command.Prepare();
			
			using var reader = await command.ExecuteReaderAsync();
			
			var magic = "";
			
			if (reader.Read()) {
				magic = reader.GetString(0);
			}
			
			reader.NextResult();
			
			if (!reader.Read()) {
				return null;
			}
			
			var track = new NTrackingInfo() {
				Ip = reader.GetString(0),
				Ua = reader.GetString(1),
				Date = reader.GetString(2)
			};
			
			return new NJustice() {
				Magic = (string) magic,
				Track = track
			};
		}
	}
}
