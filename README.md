# THIS PIECE OF SOFTWARE IS NO LONGER DEVELOPED

Reference implementation of an [N-Share] server.

[N-Share]: https://gitlab.com/kirbykevinson/n-share-client

## Dependencies

* .NET 5.0

## How to build and run

```
dotnet build
dotnet run
```

## How to configure

In order for the server to function properly, you need to set the
following environment variables:

* `N_SHARE_PREFIX` - sets the URL of the server to work on.
  `http://localhost:7043/` by default.
* `N_SHARE_ADMIN_PASSWORD` - sets the password for the super secret
  admin panel. There's no need to tell you the default because you're
  supposed to change it anyway.
