using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.Json;
using System.Text;
using System.Threading.Tasks;
using System;

using NJsonInputObject = System.Collections.Generic.Dictionary<
	string,
	System.Text.Json.JsonElement
>;
using NJsonOutputObject =
	System.Collections.Generic.Dictionary<string, object>;

using NEventHandler = System.Func<
	System.Collections.Generic.Dictionary<
		string,
		System.Text.Json.JsonElement
	>,
	System.Threading.Tasks.Task<
		System.Collections.Generic.Dictionary<string, object>
	>
>;

namespace n_share_server {
	class NListener {
		HttpListener Listener;
		
		public NListener(string prefix) {
			Listener = new HttpListener();
			
			Listener.Prefixes.Add(prefix);
			
			Listener.Start();
			
			Console.WriteLine($"Listening at {prefix}");
		}
		
		public async Task Run(Action<HttpListenerContext> requestHandler) {
			while (true) {
				var context = await Listener.GetContextAsync();
				
				requestHandler(context);
			}
		}
	}
	
	class NServer {
		NListener Listener;
		NDb Db;
		
		Dictionary<string, NEventHandler> EventHandlers;
		
		string AdminPassword;
		
		public NServer(string prefix, string adminPassword) {
			Listener = new NListener(prefix);
			Db = new NDb();
			
			EventHandlers = new Dictionary<string, NEventHandler>() {
				{"upload", OnUpload},
				{"info", OnInfo},
				{"delete", OnDelete},
				{"download", OnDownload},
				{"execution-of-justice", OnExecutionOfJustice}
			};
			
			AdminPassword = adminPassword;
		}
		
		public async Task Run() {
			Db.Run();
			
			await Listener.Run(HandleEvent);
		}
		
		NJsonOutputObject Error(string message) {
			return new NJsonOutputObject() {
				{"type", "error"},
				{"message", message}
			};
		}
		
		JsonElement StringToJsonElement(string str) {
			var json = JsonSerializer.Serialize<string>(str);
			
			var document = JsonDocument.Parse(json);
			
			return document.RootElement;
		}
		
		async void HandleEvent(HttpListenerContext context) {
			var request = context.Request;
			var response = context.Response;
			
			response.Headers.Add("Access-Control-Allow-Origin", "*");
			
			using var input = request.InputStream;
			using var output = response.OutputStream;
			
			NJsonInputObject inputJson = null;
			NJsonOutputObject outputJson = null;
			
			try {
				inputJson = await
					JsonSerializer.DeserializeAsync
					<NJsonInputObject>(input);
			} catch (JsonException) {
				outputJson = Error("invalid JSON");
				
				goto Finish;
			}
			
			if (!inputJson.ContainsKey("type")) {
				outputJson = Error("missing event type");
				
				goto Finish;
			}
			if (inputJson["type"].ValueKind != JsonValueKind.String) {
				outputJson = Error("event type isn't a string");
				
				goto Finish;
			}
			
			var eventType = inputJson["type"].GetString();
			
			if (!EventHandlers.ContainsKey(eventType)) {
				outputJson = Error("illegal event type");
				
				goto Finish;
			}
			
			inputJson["_ip"] = StringToJsonElement(request.UserHostAddress);
			inputJson["_ua"] = StringToJsonElement(request.UserAgent);
			
			outputJson = await EventHandlers[eventType](inputJson);
			
			Finish:
			
			await
				JsonSerializer.SerializeAsync
				<NJsonOutputObject>(output, outputJson);
		}
		
		async Task<NJsonOutputObject> OnUpload(NJsonInputObject e) {
			if (!e.ContainsKey("data")) {
				return Error("missing data");
			}
			if (e["data"].ValueKind != JsonValueKind.String) {
				return Error("data isn't a string");
			}
			
			if (!e.ContainsKey("magic")) {
				return Error("missing magic number");
			}
			if (e["magic"].ValueKind != JsonValueKind.String) {
				return Error("magic number isn't a string");
			}
			
			var data = e["data"].GetString();
			var magic = e["magic"].GetString();
			
			var info = new NFileInfo() {
				Name = "File",
				Description = "Sweet file",
				DownloadsLeft = 1,
				Size = 0
			};
			
			if (e.ContainsKey("name")) {
				if (e["name"].ValueKind != JsonValueKind.String) {
					return Error("name isn't a string");
				}
				
				info.Name = e["name"].GetString();
			}
			
			if (e.ContainsKey("description")) {
				if (e["description"].ValueKind != JsonValueKind.String) {
					return Error("description isn't a string");
				}
				
				info.Description = e["description"].GetString();
			}
			
			if (e.ContainsKey("downloads-left")) {
				if (e["downloads-left"].ValueKind != JsonValueKind.Number) {
					return Error("number of downloads isn't a number");
				}
				
				int downloadsLeft;
				
				if (!e["downloads-left"].TryGetInt32(out downloadsLeft)) {
					return Error("number of downloads doesn't fit 32 bits");
				}
				
				if (downloadsLeft < 1) {
					return Error("number of downloads is too low");
				}
				if (downloadsLeft > 10) {
					return Error("number of downloads is too high");
				}
				
				info.DownloadsLeft = downloadsLeft;
			}
			
			if (e.ContainsKey("size")) {
				if (e["size"].ValueKind != JsonValueKind.Number) {
					return Error("size isn't a number");
				}
				
				int size;
				
				if (!e["size"].TryGetInt32(out size)) {
					return Error("size doesn't fit 32 bits");
				}
				
				if (size < 0) {
					return Error("size is too low");
				}
				
				info.Size = size;
			}
			
			var track = new NTrackingInfo() {
				Ip = e["_ip"].GetString(),
				Ua = e["_ua"].GetString()
			};
			
			var id = await Db.Upload(data, magic, info, track);
			
			return new NJsonOutputObject() {
				{"type", "success"},
				{"id", id}
			};
		}
		
		async Task<NJsonOutputObject> OnInfo(NJsonInputObject e) {
			if (!e.ContainsKey("id")) {
				return Error("missing id");
			}
			if (e["id"].ValueKind != JsonValueKind.String) {
				return Error("id isn't a string");
			}
			
			var id = e["id"].GetString();
			
			var maybeInfo = await Db.Info(id);
			
			if (maybeInfo == null) {
				return Error("file doesn't exist");
			}
			
			var info = (NFileInfo) maybeInfo;
			
			return new NJsonOutputObject() {
				{"type", "success"},
				{"name", info.Name},
				{"description", info.Description},
				{"downloads-left", info.DownloadsLeft},
				{"size", info.Size}
			};
		}
		
		async Task<NJsonOutputObject> OnDelete(NJsonInputObject e) {
			if (!e.ContainsKey("id")) {
				return Error("missing id");
			}
			if (e["id"].ValueKind != JsonValueKind.String) {
				return Error("id isn't a string");
			}
			
			if (!e.ContainsKey("magic")) {
				return Error("missing magic number");
			}
			if (e["magic"].ValueKind != JsonValueKind.String) {
				return Error("magic number isn't a string");
			}
			
			var id = e["id"].GetString();
			var magic = e["magic"].GetString();
			
			var result = await Db.Delete(id, magic);
			
			switch (result) {
			case NDeletionResult.DoesntExist:
				return Error("file doesn't exist");
			case NDeletionResult.WrongMagic:
				return Error("wrong magic number");
			default:
				return new NJsonOutputObject() {
					{"type", "success"}
				};
			}
		}
		
		async Task<NJsonOutputObject> OnDownload(NJsonInputObject e) {
			if (!e.ContainsKey("id")) {
				return Error("missing id");
			}
			if (e["id"].ValueKind != JsonValueKind.String) {
				return Error("id isn't a string");
			}
			
			var id = e["id"].GetString();
			
			var data = await Db.Download(id);
			
			if (data == null) {
				return Error("file doesn't exist");
			}
			
			return new NJsonOutputObject() {
				{"type", "success"},
				{"data", data}
			};
		}
		
		async Task<NJsonOutputObject> OnExecutionOfJustice(
			NJsonInputObject e
		) {
			if (!e.ContainsKey("id")) {
				return Error("missing id");
			}
			if (e["id"].ValueKind != JsonValueKind.String) {
				return Error("id isn't a string");
			}
			
			if (!e.ContainsKey("admin-password")) {
				return Error("missing admin password");
			}
			if (e["admin-password"].ValueKind != JsonValueKind.String) {
				return Error("admin password isn't a string");
			}
			
			var id = e["id"].GetString();
			var adminPassword = e["admin-password"].GetString();
			
			if (adminPassword != AdminPassword) {
				return Error("wrong admin password");
			}
			
			var maybeJustice = await Db.ExecuteJustice(id);
			
			if (maybeJustice == null) {
				return Error("file haven't existed");
			}
			
			var justice = (NJustice) maybeJustice;
			
			return new NJsonOutputObject() {
				{"type", "success"},
				{"magic", justice.Magic},
				{"ip", justice.Track.Ip},
				{"ua", justice.Track.Ua},
				{"date", justice.Track.Date}
			};
		}
	}
	
	class Program {
		static async Task Main(string[] args) {
			if (args.Length != 0) {
				Console.WriteLine("usage: N_SHARE_PREFIX=[prefix] N_SHARE_ADMIN_PASSWORD=[password] n-share-server");
				
				return;
			}
			
			var prefix = Environment.GetEnvironmentVariable(
				"N_SHARE_PREFIX"
			) ?? "http://localhost:7043/";
			var adminPassword = Environment.GetEnvironmentVariable(
				"N_SHARE_ADMIN_PASSWORD"
			) ?? "please don't use this password in production";
			
			var server = new NServer(prefix, adminPassword);
			
			await server.Run();
		}
	}
}
